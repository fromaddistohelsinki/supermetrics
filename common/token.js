const fs = require('fs');
const config = require('config');
const apiHandler = require('./apiHandler');

const localToken = config.get('sl_token');

// Register and limit generating new tokens until the current one expires

const registerToken = async () => {
  const params = config.get('params');
  const { sl_token } = await apiHandler('POST', 'register', params);

  const configObj = {
    params,
    sl_token
  };
  fs.writeFileSync('./config/default.json', JSON.stringify(configObj));

  return sl_token;
};

const getToken = async () => {
  try {
    if (!localToken && !localToken.length) {
      await registerToken();
    }

    return localToken;
  } catch (error) {
    throw error.response.data;
  }
};

module.exports = { getToken, registerToken };

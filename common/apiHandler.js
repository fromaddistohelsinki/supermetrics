const axios = require('axios');

module.exports = async (method, endpoint, data) => {
  try {
    const apiUrl = 'https://api.supermetrics.com/assignment/';
    const { data: response } = await axios({
      method,
      url: `${apiUrl}${endpoint}`,
      data
    });

    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

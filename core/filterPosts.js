const moment = require('moment');

// Helper functions

const getAverage = posts =>
  Math.round(posts.reduce((a, b) => a + b, 0) / posts.length);
const longestPost = posts => posts.reduce((a, b) => (a > b ? a : b));
const getMonth = date => moment(date.split('T')[0]).format('MMMM');
const getWeek = date => moment(date.split('T')[0]).format('WW');
const getValues = values => [...Object.values(values)];

const getPostsLengthByGroup = (postsData, group) => {
  return postsData.reduce((posts, currentPost, i) => {
    const { created_time, message } = currentPost;
    const month = group(created_time);

    return {
      ...posts,
      [month]: [...(posts[month] || []), message.length]
    };
  }, {});
};

const filterAverages = postsData => {
  const postsByMonth = getPostsLengthByGroup(postsData, getMonth);

  return callback =>
    getValues(postsByMonth).map((item, index) => {
      const month = Object.keys(postsByMonth)[index];

      return {
        [month]: callback(item)
      };
    });
};

// Average character length of a post / month

const averageCharactersByMonth = postsData =>
  filterAverages(postsData)(getAverage);

// The Longest post by character length / month

const longestPostByMonth = postsData => filterAverages(postsData)(longestPost);

// The Total posts split by week

const totalPostsByWeek = postsData => {
  const postsList = postsData.reduce((posts, currentPost) => {
    const { created_time, id } = currentPost;
    const week = getWeek(created_time);

    (posts[week] = posts[week] || []).push(id);

    return posts;
  }, {});

  return getValues(postsList).map((post, i) => {
    const week = Object.keys(postsList)[i];

    return {
      [week]: getValues(post).length
    };
  });
};

// The Average number of posts per user / month

const monthlyPostsByUser = postsData => {
  return Object.entries(
    postsData.reduce((posts, currentPost) => {
      const { created_time, from_id } = currentPost;
      const month = getMonth(created_time);

      (posts[month] = posts[month] || []).push(from_id);

      return posts;
    }, {})
  ).map((item, i) => {
    const posts = item[1].length;
    const users = [...new Set(item[1])].length;
    const month = item[0];

    return {
      [month]: posts / users
    };
  });
};

module.exports = {
  averageCharactersByMonth,
  longestPostByMonth,
  totalPostsByWeek,
  monthlyPostsByUser
};

const apiHandler = require('../common/apiHandler');
const { getToken, registerToken } = require('../common/token');

const getPostsByPage = async page => {
  try {
    const token = await getToken();
    const { posts } = await apiHandler(
      'GET',
      `posts?sl_token=${token}&page=${page}`
    );

    return posts;
  } catch (error) {
    if (error) {
      const { sl_token } = await registerToken();
      const { posts } = await apiHandler(
        'GET',
        `posts?sl_token=${sl_token}&page=${page}`
      );

      return posts;
    }
  }
};

// Get all post from all pages

const getAllPosts = async () => {
  let posts = [];
  let totalPageNumber = 10;
  let page = 1;

  while (page <= totalPageNumber) {
    let postInPage = await getPostsByPage(page);
    page += 1;
    posts = posts.concat(postInPage);
  }

  return posts;
};

module.exports = getAllPosts;

const express = require('express');
const getAllPosts = require('./core/getPosts');
const filterPosts = require('./core/filterPosts');
const PORT = process.env.PORT || 5000;

const app = express();

app.use(express.json({ extended: false }));

app.get('/', async (req, res, next) => {
  try {
    const postsData = await getAllPosts();

    if (postsData.includes(undefined)) throw new Error('No Data');

    const response = {
      averageCharactersByMonth: filterPosts.averageCharactersByMonth(postsData),
      longestPostByMonth: filterPosts.longestPostByMonth(postsData),
      totalPostsByWeek: filterPosts.totalPostsByWeek(postsData),
      monthlyPostsByUser: filterPosts.monthlyPostsByUser(postsData)
    };

    res.json(response);
  } catch (error) {
    next(error);
  }
});

app.get('/posts', async (req, res, next) => {
  try {
    const postsData = await getAllPosts();

    if (postsData.includes(undefined)) throw new Error('No Data');

    res.json(postsData);
  } catch (error) {
    next(error);
  }
});

app.listen(PORT, () => {
  console.log(`App listening at ${PORT}`);
});
